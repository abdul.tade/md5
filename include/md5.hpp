#pragma once

#include "buffer.hpp"
#include "hexconverter.hpp"

class MD5
{
public:
    MD5()
    {
        initializeMDBuffer();
    }

    ~MD5() = default;

    Buffer<uint8_t> preprocessString(const std::string &str)
    {
        auto ptr = reinterpret_cast<uint8_t *>(
            const_cast<char *>(str.c_str()));
        auto oldSize = str.length();

        Buffer<uint8_t> buffer{ptr, str.length()};
        buffer.pad(64);
        buffer.data()[oldSize] = 1;

        std::memset(
            buffer.data() + oldSize + 1, 0x0,
            buffer.size() - oldSize - 1);

        uint64_t *lastWord = reinterpret_cast<uint64_t *>(buffer.data() + buffer.size() - 8);
        *lastWord = oldSize;

        return buffer;
    }

    std::string hash(const std::string &str)
    {
        auto buffer = preprocessString(str);
        auto blockCount = buffer.size() / 16;
        uint32_t *blockPtr = reinterpret_cast<uint32_t *>(buffer.data());

        size_t A = consts_[0];
        size_t B = consts_[1];
        size_t C = consts_[2];
        size_t D = consts_[3];

        for (size_t k = 0; k < blockCount; k++)
        {
            uint32_t *block = blockPtr + (16 * k);
            for (size_t i = 0; i < 64; i++)
            {
                size_t F = 0, g = 0;
                size_t shiftAddrIndex = 0;
                if (i >= 0 && i <= 15)
                {
                    F = (consts_[1] & consts_[2]) | ((~consts_[1]) & consts_[3]);
                    g = i;
                    shiftAddrIndex = 0;
                }
                else if (i >= 16 && i <= 31)
                {
                    F = (consts_[3] & consts_[2]) | ((~consts_[3]) & consts_[2]);
                    g = ((5 * i) + 1) % 16;
                    shiftAddrIndex = 1;
                }
                else if (i >= 32 && i <= 47)
                {
                    F = consts_[1] ^ consts_[2] ^ consts_[3];
                    g = ((3 * i) + 5) % 16;
                    shiftAddrIndex = 2;
                }
                else if (i >= 48 && i <= 63)
                {
                    F = consts_[2] ^ (consts_[1] | (~consts_[3]));
                    g = (7 * i) % 16;
                    shiftAddrIndex = 3;
                }

                F = F + A + K[i] + block[g];
                A = D;
                D = C;
                C = B;
                B += leftRotate(F, shiftArr[shiftAddrIndex][i%4]);
            }

            consts_[0] += A;
            consts_[1] += B;
            consts_[2] += C;
            consts_[3] += D;
        }

        HexConverter cvt{};
        std::string result{};

        for (size_t i = 0; i < 4; i++)
        {
            result += cvt.fromInt(consts_[i]);
        }

        return result;
    }

private:
    uint32_t consts_[4];
    uint32_t K[64] = {
        0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
        0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
        0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
        0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
        0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
        0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
        0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
        0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
        0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
        0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
        0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
        0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
        0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
        0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
        0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
        0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
    };
    uint32_t shiftArr[4][4] = {
        {7, 12, 17, 22},
        {5, 9, 14, 20},
        {4, 11, 16, 23},
        {6, 10, 15, 21}};

    void initializeMDBuffer()
    {
        consts_[0] = 0x67425301; /*A*/
        consts_[1] = 0xefcdab89; /*B*/
        consts_[2] = 0x98badcfe; /*C*/
        consts_[3] = 0x10325476; /*D*/
    }

    template <typename T>
    T absolute(T t)
    {
        return (t < 0) ? -t : t;
    }

    void populateConstArr()
    {
        for (size_t i = 0; i < 64; i++)
        {
            K[i] = static_cast<uint32_t>(floor((2ul << 32) * absolute(sin(i + 1))));
        }
    }

    template <typename T>
    T leftRotate(T num, size_t times)
    {
        return (num << times) | (num >> ((8ul * sizeof(T)) - times));
    }
};