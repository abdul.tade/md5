CXX:= clang++
CXXFLAGS := -g -Wall

.PHONY: all build run

all:
	make build
	make run

build: src/main.cpp build/

	$(CXX) $(CXXFLAGS) -o build/main src/main.cpp -std=c++20

run: build/main

	echo "running build/main"
	./build/main