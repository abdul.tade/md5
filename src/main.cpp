#include "../include/md5.hpp"

int main()
{
    std::string msg = "The quick brown fox jumps over the lazy dog";
    MD5 hasher{};
    auto result =  hasher.hash(msg);

    std::cout << result << '\n';
}